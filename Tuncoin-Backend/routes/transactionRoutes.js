/***** GLOBAL IMPORTS *****/
const { hash } = require('bcrypt');
const express = require('express');
const router = express.Router();

/***** FUNCTIONS IMPORT *****/
const { transaction, transactionMobile, buy, buyMobile, history, historyMobile, getBalance, getBalanceMobile } = require('../functions/transaction');

/***** METHODES GOES HERE *****/
/** Transaction **/
/* WEB */
router.post('/transaction', transaction);
router.post('/buy', buy);
router.get('/history', history);
router.get('/getBalance', getBalance);
/* MOBILE */
router.post('/m/transaction', transactionMobile);
router.post('/m/buy', buyMobile);
router.get('/m/history', historyMobile);
router.get('/m/getBalance', getBalanceMobile);

module.exports = router;